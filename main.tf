provider "aws" {
  region="ap-southeast-1"
}

resource "aws_sqs_queue" "antManAppQueue-dlq" {
  name="antman-app-queue-dlq"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
}

resource "aws_sqs_queue" "antManAppQueue" {
  name="antman-app-queue"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
  redrive_policy            = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.antManAppQueue-dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "antManDocQueue-dlq" {
  name="antman-doc-queue-dlq"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
}

resource "aws_sqs_queue" "antManDocQueue" {
  name="antman-doc-queue"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
  redrive_policy            = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.antManDocQueue-dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "antManNcbQueue-dlq" {
  name="antman-ncb-queue-dlq"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
}

resource "aws_sqs_queue" "antManNcbQueue" {
  name="antman-ncb-queue"
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 2
  redrive_policy            = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.antManNcbQueue-dlq.arn}\",\"maxReceiveCount\":4}"
}

